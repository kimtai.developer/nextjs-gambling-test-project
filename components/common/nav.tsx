'use client'
 
import { usePathname } from 'next/navigation'
import Link from 'next/link'
import React from 'react'

const Nav = () => {
    const pathname = usePathname();

	return (
		<div className='nav-custom'>
            <ul className="nav ">
                <li className="nav-item"><Link className={`nav-link ${pathname === '/' ? 'active' : ''}`} href='/'>Home</Link></li>
                <li className="nav-item"><Link className={`nav-link ${pathname === '/jackpot' ? 'active' : ''}`} href='/jackpot'>Jackpot</Link></li>
                <li className="nav-item"><Link className={`nav-link ${pathname === '/casino' ? 'active' : ''}`} href='/casino'>Casino</Link></li>
                <li className="nav-item"><Link className={`nav-link ${pathname === '/jetx' ? 'active' : ''}`} href='/jetx'>JetX</Link></li>
                <li className="nav-item"><Link className={`nav-link ${pathname === '/aviatrix' ? 'active' : ''}`} href='/aviatrix'>Aviatrix</Link></li>
            </ul>
        </div>
	)
}

export default Nav