import React from 'react';

const FooterNav = () => {
    return (
        <div className='fixed-footer d-block d-sm-block d-md-none'>
            <div className="d-flex justify-content-around align-items-center">
                <div className="d-flex flex-column align-items-center justify-content-center">
                    <i className="icofont-ui-home mr-0"></i>
                    <span className="mt-1">Home</span>
                </div>
                <div className="d-flex flex-column align-items-center justify-content-center">
                    <i className="icofont-diamond mr-0"></i>
                    <span className="mt-1">Live</span>
                </div>
                <div className="d-flex flex-column align-items-center justify-content-center" >
                    <span className="betslip d-flex  align-items-center justify-content-center">0</span>
                </div>
                <div className="d-flex flex-column align-items-center justify-content-center">
                    <i className="icofont-link-alt mr-0"></i>
                    <span className="mt-1">Bets</span>
                </div>
                <div className="d-flex flex-column align-items-center justify-content-center">
                    <i className="icofont-ui-user mr-0"></i>
                    <span className="mt-1">Profile</span>
                </div>
            </div>
        </div>
    );
}

export default FooterNav;