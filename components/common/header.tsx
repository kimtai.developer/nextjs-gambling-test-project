import Link from 'next/link';


const Header = () => {
    
    const token = false;

    return (
        <header className="top-header">
            <div className="header">
                <Link href="/" className="home-link">
                    <img src="/images/logo.png" alt="Logo" className="img-fluid" style={{ height: 40 }} />
                </Link>

                { !token && <div className="top-nav">
                    <ul className="nav">
                        <li className="nav-item"><Link className="nav-link" href='/login'>Login</Link></li>
                        <li className="nav-item"><Link className="nav-link" href='/register'>Register</Link></li>
                    </ul>
                </div>
                }

                { token && <div className="top-nav">
                    <ul className="nav">
                        <li className="nav-item"><Link className="nav-link" href='/my-bets'>Bets</Link></li>
                        <li className="nav-item"><Link className="nav-link" href='/profile'>Profile</Link></li>
                    </ul>
                </div>
                }
            </div>
        </header>
    )
}

export default Header