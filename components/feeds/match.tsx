// 'use client';
import { useRouter } from 'next/navigation';
import { Market ,Match, MatchMarket} from "@/models/model";
import { format } from "date-fns";

type Props = { match: Match, markets: Market[] }


const MatchCard = ( { match, markets }: Props ) => {

    // const router = useRouter(); onClick={() => router.push(`/single-match/${match.id}`)}
    
    const filterMatch = (arr: MatchMarket,id: number) => {
        return arr.odds!.find( el => el.outcomeId === parseInt(id.toString()));
    }

    return (
        <div className="sport-header px-2 px-md-0" >
            <div className="market-header teams-header" > 
                <div >
                    <span className='time mr-2'>{ format(match.scheduled,'hh:ss • dd/MM')}</span>  
                    <span className='more-markets' >
                        <i className="icofont-bubble-right"></i>+{ match.marketCount.toString()} Markets
                        </span>
                </div>
                <span className='font-semi-bold'>{ match.home }</span>
                <span className='font-semi-bold'>{ match.away }</span>
            </div>
            <div className="markets-body">
                {
                    markets!.map( el =>  {
                    const arr = match.markets.find( x => x.marketId === el.id.toString() && el.handicap === x.handicap) as MatchMarket;
                    if(arr) {
                        return <div key={el.id.toString()}  className={`markets-header ${el.selections.length === 3 ? 'three' : 'two'} ${ el.mobile ? '' : 'd-none d-sm-none d-md-block' }`}>
                            <div className="markets-odd">
                                {
                                el.selections.map( (elem : any) => {
                                    if( filterMatch(arr,elem.id) !== null ) {
                                    const ab = filterMatch(arr,elem.id);
                                    return  <span key={elem.id.toString()} className={`span-odds`}>{ ab!.odds.toFixed(2) }</span>
                                    } else {
                                    return  <span key={elem.id.toString()} className="span-odds"><i className="icofont-lock"></i></span>;
                                    }
                                })
                                }
                            </div>
                        </div>;
                    } else {
                        return <div key={el.id.toString()}  className={`markets-header ${el.selections.length === 3 ? 'three' : 'two'} ${ el.mobile ? '' : 'd-none d-sm-none d-md-block' }`} >
                            <div className="markets-odd">
                                {
                                    el.selections.map( (elem : any) => 
                                        <span key={elem.id.toString()} className="span-odds"><i className="icofont-lock"></i></span>
                                    )
                                }
                            </div>
                        </div>
                    }
                    })
                }
            </div>
        </div> 
    );
}

export default MatchCard;