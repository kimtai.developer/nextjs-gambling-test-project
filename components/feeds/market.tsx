import { Sport } from "@/models/model";

type Props = { sport: Sport }

const Market = ({ sport } : Props) => {
    return (
        <div className="sport-header px-2 px-md-0">
            <div className="market-header">
                <span className="font-semi-bold">{ sport.name }</span>
            </div>
            <div className="markets-body">
                {
                    sport.markets.map( el => 
                        <div key={el.id.toString()}  className={`markets-header ${el.selections.length === 3 ? 'three' : 'two'} ${ el.mobile ? '' : 'd-none d-sm-none d-md-block' }`}>
                            <p>{ el.name }</p>
                            <div className="markets-odd">
                                {
                                    el.selections.map( elem => 
                                        <span key={elem.id.toString()} className="">{ elem.name }</span>
                                    )
                                }
                            </div>
                        </div>
                    )
                }
            </div>
        </div>
    )
}

export default Market