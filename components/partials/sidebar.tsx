import React from 'react';
import { getMenu } from '@/services/api';
import { Menu } from "@/models/model";

const Sidebar = async () => {
    const id: number = 10;
    let menu: Menu[] = [];

    await getMenu().then(res => {
        menu = res;
    });
    
    return (
        <div className="">
        <ul className="list-unstyled sport-list">
            {
                menu.map( el => <li className={ `font-semi-bold ${ id === el.id ? 'text-yellow' : '' }`} key={el.id.toString()} >{ el.name}</li>)
            }
        </ul>
    </div>
    )
}

export default Sidebar