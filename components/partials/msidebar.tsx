import { Menu } from '@/models/model';
import { getMenu } from '@/services/api';
import { getIcon } from '@/utils/utils';
import React from 'react';

const MSidebar = async () => {

    const id: number = 10;
    let menu: Menu[] = [];

    await getMenu().then(res => {
        menu = res;
    });
    
    return (
        <div className="d-block d-sm-block d-md-none">
            <div className="m-sidebar">
                {
                    menu.map( el => 
                        <div className={ `d-flex flex-column align-items-center ${ id === el.id ? 'text-yellow' : '' }`} key={el.id.toString()}>
                            <i className={`icofont-${ getIcon(el.id)}`}></i>
                            <span className="sport-name">{ el.name}</span>
                        </div>
                    )
                }
            </div>
        </div>
    );
}

export default MSidebar;