import React from 'react'

const CasinoSidebar = () => {
    
    const list = [
        'Popular', 'New', 'New Games', 'Drops and Wins', 'Slots', 'Table Games', 'Live Casino', 'Crash Games','Livespins', 'Blackjacks','Jackpot Slots', 'All Games'
    ];
    
    return (
        <div className=''>
            <ul className="list-unstyled sport-list">
            {
                list.map( (el,i) => <li key={ i } className={ `font-semi-bold ${ i === 0 ? 'text-yellow' : '' }`}>{ el }</li>)
            }   
            </ul>
        </div>
    )
}

export default CasinoSidebar