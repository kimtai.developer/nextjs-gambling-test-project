
import { combineReducers } from '@reduxjs/toolkit';
import gamesSlice from './games/gamesSlice';
import authSlice from './auth/authSlice';

const rootReducer = combineReducers({
    games: gamesSlice,
    auth: authSlice
});

export default rootReducer;