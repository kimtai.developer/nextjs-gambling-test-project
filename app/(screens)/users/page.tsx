import Link from "next/link";
import React, { useState } from 'react';
import { getSport } from '@/services/api';
import { Match } from "@/models/model";

interface User {
    id: number,
    name: string,
    email: string
}

const Users = async () => {

    let feeds: Match[] = []
    const res = await fetch('https://jsonplaceholder.typicode.com/users', { cache : 'no-store'});
    const users: User[] = await res.json();

    await getSport(10).then( res => {
        feeds = res
    } ).catch( err => console.log('Error--',err) );

    return (
        <>  
            <div className="container">
                <Link href="/">Back Home</Link>
                <p className='display-4'>List of Users</p>
                <ul className="list-unstyled">
                    { feeds.map( match => <li key={ match.id}>{match.home} - {match.away}</li>)}
                </ul>
            </div>
        </>
    )
}

export default Users