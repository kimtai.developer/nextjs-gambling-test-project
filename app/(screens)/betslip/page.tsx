'use client';
import { useRouter } from 'next/navigation';
import React from 'react'

type Props = { type: String }

const Betslip = ({ type } : Props) => {
    const router = useRouter();
    return (
        <>
            <div className="betslip-header font-semi-bold">
                <p>Betslip</p>
                <p>Clear</p>
                {/* {  betslip.length > 0  && type === 'Pre-Match' && <p className='clear' onClick={ () => dispatch( clearBetslip() ) }>Clear </p> }
                {  jackpot.length > 0  && type === 'Jackpot' && <p className='clear' onClick={ () => dispatch( clearJackpotSlip() ) }>Clear </p> } */}
            </div>
            <div className='no-betslip'>
                <p className='font-semi-bold'>You have not selected any jackpot bet</p>
                <span className=''>Make your first pick to start playing.</span>
            </div>

            <div className='no-betslip'>
                <p className='font-semi-bold'>You have not selected any bet</p>
                <span className=''>Make your first pick to start playing.</span>
            </div>

            <img src="/images/casino.png" alt="Home" className="img-fluid rounded mt-3 mb-3 d-none d-sm-none d-md-block" onClick={ () => router.push('/casino') }/>
            <img src="/images/jackpot.png" alt="Home" className="img-fluid rounded d-none d-sm-none d-md-block" onClick={ () => router.push('/jackpot') }/>
            
        </>
    )
}

export default Betslip