import React from 'react';
import Betslip from '../../betslip/page';
import Sidebar from '@/components/partials/sidebar';
import { Match } from '@/models/model';
import moment from 'moment';
import { getSingleMatch } from '@/services/api';

const SingleMatch = async ( {params }: { params : {id: string}}) => {
    let match: Match = {} as Match;

    await getSingleMatch().then( res => {
        match = res;
    })
    return (
        <> 
            <div className="wrapper p-md-3">
                <div className="main-feeds">

                    {/* ---Sidebar--- */}
                    <div className="sidebar d-none d-sm-none d-md-block">
                        <Sidebar />
                    </div>

                    {/* ---Main--- */}
                    <div className="main-home px-3 px-md-0">
                    {
                        Object.keys(match).length > 0 && <>
                            <div className="single-match-header">
                                <p >
                                    <i className="icofont-bubble-left"></i>
                                    <span className="font-semi-bold">Back { params.id } </span>
                                </p>
                                <p className="font-semi-bold">
                                    { match.sportName }, { match.country } • { match.tournamanent}
                                </p>
                            </div>

                            <div className="single-match-teams">
                                <div className="team">
                                    <i className="icofont-cop-badge"></i>
                                    <p className='font-semi-bold'>{ match.home }</p>
                                </div>
                                <div className="team">
                                    <p className='time'>{  moment(`${match.scheduled}`).format('hh:ss • DD/MM') }</p>
                                </div>
                                <div className="team">
                                    <i className="icofont-cop-badge"></i>
                                    <p className='font-semi-bold'>{ match.away }</p>
                                </div>
                            </div>

                            <br />
                            
                            {  match && match.markets.map( el => {
                                    return <div className="single-market-wrapper" key={el.id}>
                                        <p className="market-heder">
                                            <i className="icofont-shield-alt"></i>
                                            <span className="font-semi-bold">{ el.name }</span>
                                        </p>
                                        <div className={`market-odds ${el.odds.length > 2 ? 'three-columns' : 'two-columns'} `}>

                                            { el.odds.map( elem => {
                                                return <div className={`odds`} key={elem.id} >
                                                    <span>{ elem.name }</span>
                                                    <span>{ elem.odds.toFixed(2) }</span>
                                                </div>
                                            }) }
                                        </div>
                                    </div>;
                                })  
                            }
                        </>
                    }                        
                    </div>

                </div>

                {/* ---Betslip--- */}
                <div className="betslip d-none d-sm-none d-md-block">
                    <Betslip type="Pre-Match" />
                </div>

            </div>
        </>
    )
}

export default SingleMatch;