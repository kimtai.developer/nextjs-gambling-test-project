import { Footer,Header,Nav,FooterNav} from "@/components/common";
import StoreProvider from "../storeProvider";

export default function AuthLayout( { children }: Readonly<{ children: React.ReactNode; }> ) { 
    return (
        <StoreProvider>
            <Header />
            <Nav />
            { children }
            <Footer />
            <FooterNav />
        </StoreProvider>
    )
}