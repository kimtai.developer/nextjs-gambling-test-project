import Footer from '@/components/common/footer'
import Header from '@/components/common/header'
import Nav from '@/components/common/nav'
import React from 'react';
import { Aviatrix } from "@/models/model";

const Aviatrix = () => {

    const aviatrix: Aviatrix = {
        url: 'https://game.aviatrix.bet/',
        key: 'aee2e5f0-a356-45c9-abe9-c28b502fee98',
        cid:'rahisibet',
        lobbyUrl: 'https://rahisibet.com',
        isDemo:true,
        isFull:true,
    }
    const url: string = `${ aviatrix.url }?cid=${ aviatrix.cid }&productId=nft-aviatrix&lobbyUrl=${ aviatrix.lobbyUrl }&isDemo=${ aviatrix.isDemo }&lang=en`;
    
    return (
        <>
            <div className="iframe-wrapper">
                <iframe id="aviatrixiframe" title="Aviatrix Game" src={ url } allow="autoplay;clipboard-write;fullscreen"></iframe>
            </div>
        </>
        
    )
}

export default Aviatrix