import { Sport } from "@/models/model"

type Props = { sport: Sport }

const Market = ({ sport } : Props) => {
    return (
        <div className="sport-header jackpot mt-1 px-2 px-md-0">
            <div className="market-header">
                <span className="font-semi-bold">{ sport.name }</span>
            </div>
            <div className="markets-body">
                {
                    sport.markets.map( el =>  {

                        if(el.id === 2) {
                            return <div key={el.id.toString()}  className={`markets-header jackpot ${el.selections.length === 3 ? 'three' : 'two'}`}>
                                <p>{el.name}</p>
                                <div className="markets-odd jackpot">
                                    {
                                        el.selections.map( elem => 
                                            <span key={elem.id.toString()} className="">{ elem.name }</span>
                                        )
                                    }
                                </div>
                            </div>
                        } else {
                            return '';
                        }
                        
                    })
                }
            </div>
        </div>
    )
}

export default Market