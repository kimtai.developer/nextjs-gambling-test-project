import { Match, MatchMarket } from '@/models/model';
import moment from 'moment';

type Props = { match: Match }

const Main = ({ match}: Props) => {

    const filterMatch = () => {
        return match.markets.find( el => el.marketId === '2') || null
    }

    return (
        <div className="sport-header jackpot px-2 px-md-0">
            <div className="market-header teams-header">
                <div>
                    <span className='time'>
                        {/* <ReactCountryFlag countryCode={ flags[match.country]} svg className="mr-2 mb-1"/> */}
                        { moment(`${match.scheduled}`).format('hh:ss • DD/MM') }
                    </span>
                </div>
                <span className='font-semi-bold'>{ match.home }</span>
                <span className='font-semi-bold'>{ match.away }</span>
            </div>
            <div className="markets-body">
                <div className={`markets-header jackpot`}>
                    <div className="markets-odd jackpot">
                    {
                        filterMatch()!.odds.map( (elem : any) => {  
                            return  <span key={elem.id.toString()} className={`span-odds `}>{ elem.odds.toFixed(2) }</span>
                        })
                    }
                    </div>
                </div>
            </div>
        </div>  
    )
}

export default Main