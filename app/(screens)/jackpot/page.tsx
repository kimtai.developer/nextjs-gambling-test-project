import { Match, Sport } from "@/models/model";
import { getJackpot } from "@/services/api";
import { displayMarkets } from "@/models/market";
import Market from "./market";
import Betslip from "../betslip/page";
import Main from "./match";
import { MSidebar,Sidebar } from "@/components/partials";

const Jackpot = async () => {
    
    let feeds: Match[] = [];
    const sport = displayMarkets.find( el => el.id === 10) as Sport;
    await getJackpot().then( res => { feeds = res});

    return (
        <>
            <div className="wrapper p-md-3">
                <div className="main-feeds">
                    
                    {/* ---Sidebar--- */}
                    <div className="sidebar d-none d-sm-none d-md-block">
                        <Sidebar />
                    </div>

                    <MSidebar />

                    {/* ---Main--- */}
                    <div className="main-home mb-5">

                        {/* ---Carousel--- */}
                        <img src="/images/image3.png" alt="Home" className="img-fluid"/>
                        
                        {/* Auto Select */}
                        <div className="py-md-2 mt-2 text-right p-2 px-md-0">
                            <button className="btn btn-theme font-semi-bold">Auto Select</button>
                        </div>

                        
                        {/* ---Markets--- */}
                        {  typeof sport === 'object' &&  <Market sport={sport}/> }

                        {/* ---Matches--- */}
                        { feeds.length > 0 && typeof sport === 'object' && feeds.slice().sort( (a,b) => new Date(a.scheduled).valueOf() - new Date(b.scheduled).valueOf() ).map( x => <Main key={ x.id.toString() }  match={x} />) }
                        
                    </div>
                </div>

                {/* ---Betslip--- */}
                <div className="betslip d-none d-sm-none d-md-block">
                    <Betslip type='Jackpot'/>
                </div>
            </div>
        </>

    )
}

export default Jackpot