import Market from "../../components/feeds/market";
import { Match, Sport } from "@/models/model";
import { getSport } from "@/services/api";
import { displayMarkets } from "@/models/market";
import MatchCard  from "@/components/feeds/match";
import Betslip from "./betslip/page";
import { MSidebar,Carousel,Sidebar } from "@/components/partials";

export default async function Home() {

    let feeds: Match[] = [];
    await getSport(10).then( res => {
        feeds = res
    } ).catch( err => console.log('Error--',err) );
    const sport = displayMarkets.find( el => el.id === 10) as Sport;

    return (
        <>
            <div className="wrapper p-md-3">
                <div className="main-feeds">

                    {/* ---Sidebar--- */}
                    <div className="sidebar d-none d-sm-none d-md-block">
                        <Sidebar />
                    </div>

                    <MSidebar />

                    {/* ---Main--- */}
                    <div className="main-home mb-5">

                        {/* ---Carousel--- */}
                        <Carousel />

                        {/* ---Markets--- */}
                        {  feeds.length > 0 && <Market sport={sport}/> }
                        
                        {/* ---Matches--- */}
                        { feeds.length > 0 &&  feeds.slice().sort( (a,b) => new Date(a.scheduled).valueOf() - new Date(b.scheduled).valueOf() ).map( x => <MatchCard key={ x.id.toString() }  match={x} markets={ sport.markets }/>) }

                    </div>
                </div>

                {/* ---Betslip--- */}
                <div className="betslip d-none d-sm-none d-md-block">
                    <Betslip type='Pre-Match'/>
                </div>
            </div>
        </>
    );
}
