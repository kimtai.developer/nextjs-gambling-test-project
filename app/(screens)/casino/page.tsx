import Footer from '@/components/common/footer';
import Header from '@/components/common/header';
import Nav from '@/components/common/nav';
import CasinoSidebar from '@/components/partials/casinoSidebar';
import React from 'react';

interface Info {
    image: string,
    name: string,
}

const Casino = () => {

    const casinoGames = [
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vs10egrich.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name: 'Queen of Gods',
        },
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vs20amuleteg.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name: 'Fortune of Gaza',
        },
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vswayszombcarn.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name: 'Zombie Carnival',
        },
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vs20cleocatra.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name: 'Cleo Catra',
        },
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vs20gobnudge.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name: 'Goblin Heist Powernudge',
        },
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vs20drtgold.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name: 'Drill That Gold',
        },
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vswayslions.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name: '5 Lions Megaways',
        },
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vswaysbbb.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name: 'Big Bass Bonanza Megaways',
        },
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vswaysbufking.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name: 'Buffalo King Megaways',
        },
        {
          image: 'https://rahisibetsu-dk2.pragmaticplay.net/game_pic/rec/325/vswaysxjuicy.png?secureLogin=rhsbts_rahisibet&hash=bf55710e3bc11b8b48f00b63dc9c279b',
          name:  'Extra Juicy Megaways',
        }
    ];
    let data: Info[] = casinoGames;

    // const [data,setData] = useState(casinoGames);

    const search = (name: string) => {
        let arr;
        if(name !== '') {
            var regexLiteral = new RegExp(`${name}`,'i');
            arr =  casinoGames.filter( el => regexLiteral.test(el.name) )
        } else {
            arr = casinoGames;
        }
        data = arr;
        // setData(arr);
        return arr;
    }

    return (
      <>
        {/* Wrapper */}
        <div className="wrapper p-md-3">
            <div className="main-feeds">

                {/* ---Sidebar--- */}
                <div className="sidebar d-none d-sm-none d-md-block">
                    <CasinoSidebar />
                </div>

                {/* Body */}
                {/* onChange={ (e) => search(e.target.value)} */}
                <div className="main-home mb-5">
                    <div className="casino-search px-2 px-md-0">
                        <input type="text" className="form-control" placeholder='Search Games' />
                    </div>

                    <div className="casino-grid p-2 p-md-0">
                        {/* ----Casino Game----- */}
                        { [...Array(data.length)].map((x, i) => {
                        return <div className="casino-card" key={ `c1${i}`}>
                            <div className="casino-card-overlay">
                            <img src={ data[i].image } alt="Alt" className="img-fluid" />
                            <div className="casino-game-overlay">
                                <button data-v-4e193da9="" className="btn btn-sm bg-theme mb-2">Launch demo</button>
                                <button data-v-4e193da9="" className="btn btn-sm bg-theme">Play the game</button>
                            </div>
                            </div>
                            <div  className="casino-game-info">
                            <i className="icofont-shield-alt"></i>
                            <span className='font-semi-bold'>{ data[i].name }</span>
                            </div>
                        </div> } )
                        }
                        { [...Array(data.length)].map((x, i) => {
                        return <div className="casino-card" key={ `c2${i}`}>
                            <div className="casino-card-overlay">
                            <img src={ data[i].image } alt="Alt" className="img-fluid" />
                            <div className="casino-game-overlay">
                                <button data-v-4e193da9="" className="btn btn-sm bg-theme mb-2">Launch demo</button>
                                <button data-v-4e193da9="" className="btn btn-sm bg-theme">Play the game</button>
                            </div>
                            </div>
                            <div  className="casino-game-info">
                            <i className="icofont-shield-alt"></i>
                            <span className='font-semi-bold'>{ data[i].name }</span>
                            </div>
                        </div> } )
                        }
                    </div>
                </div>
            </div>
        </div>
      </>
    )
}

export default Casino