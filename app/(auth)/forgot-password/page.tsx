import Link from 'next/link'
import React from 'react'

const ForgotPassword = () => {
    return (
        <>
            <h4 className="display-5">Forgot Password </h4>
            {/* <p className="text-muted mb-4">Reset password request will be send to entered email.</p> */}
            <form>
                <div className="form-group mb-3">
                    <label htmlFor="">Email:</label>
                    <input id="inputEmail" type="email" placeholder="Email" required className="form-control" />
                </div>

                <div className="form-group mb-3">
                    <button type="submit" className="btn btn-default-theme btn-block text-uppercase mb-2">Send</button>
                </div>
            </form>
            <div className="d-flex justify-content-end py-1 align-content-center">
                <Link href="/login">Login</Link>
            </div>
        </>
    )
}

export default ForgotPassword