import React from 'react';

const Register = () => {
    return (
        <>  
            <h4 className="display-5">Register </h4>
            {/* <p className="text-muted mb-4">Create a login split page using Bootstrap 4.</p> */}
            <form>
                <div className="form-group mb-3">
                    <label htmlFor="">Email:</label>
                    <input type="email" placeholder="Email" required className="form-control" v-model="email" />
                </div>
                <div className="form-group mb-3">
                    <label htmlFor="">Password:</label>
                    <input type="password" placeholder="Password" required className="form-control" v-model="password" />
                </div>
                <div className="form-group mb-3">
                    <label htmlFor="">Confirm Password:</label>
                    <input type="password" placeholder="Password" required className="form-control" v-model="confirm_password" />
                </div>
                <br />
                <div className="form-group mb-3">
                    <button type="submit" className="btn btn-default-theme btn-block  mb-2">Sign Up</button>
                </div>
            </form>
        </>
    )
}

export default Register;