import Link from 'next/link'
import React from 'react'

const Login = () => {
    return (
        <>
            <h4 className="display-5">Login </h4>
            <form>
                <div className="form-group mb-3">
                    <label htmlFor="">Phone Number:</label>
                    <input id="inputEmail" type="text" placeholder="0722-123-456" name="phone" className="form-control"/>
                </div>
                <div className="form-group mb-3">
                    <label htmlFor="">Password:</label>
                    <input id="inputPassword" type="password" placeholder="Password" name="pin" className="form-control"/>
                </div>
                <div className="custom-control custom-checkbox mb-3">
                    <input id="customCheck1" type="checkbox" className="custom-control-input"/>
                    <label htmlFor="customCheck1" className="custom-control-label">Remember me</label>
                </div>
                <br />
                <div className="form-group mb-3">
                    <button type="submit" className="btn btn-default-theme btn-block mb-2">Submit</button>
                </div>
            </form>
            <div className="d-flex justify-content-end py-1 align-content-center">
                <Link href="/forgot-password">Forgot Password?</Link>
            </div>
        </>
    )
}

export default Login