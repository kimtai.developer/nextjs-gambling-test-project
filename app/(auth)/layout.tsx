import Link from "next/link";
import type { Metadata } from "next";

export const metadata: Metadata = {
    title: "Homesport Auth",
    description: "Kenya's No 1 Betting site with awesome user experience",
  };

export default function AuthLayout({ children }: Readonly<{ children: React.ReactNode; }>) {
    return (
        <div className="container-fluid">
            <div className="row no-gutter">
               
                {/* The content half */}
                <div className="col-md-6 auth-background">
                    <div className="login d-flex align-items-center py-5">

                        <div className="container">
                            <div className="row">
                                <div className="col-lg-10 col-xl-8 mx-auto">
                                    <h2 className="display-4">
                                        <Link href="/" className="home-link">
                                            <img src="/images/logo.png" alt="" className="img-fluid" />
                                        </Link>
                                    </h2>
                                    <hr />
                                    { children }
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {/* The image half */}
                <div className="col-md-6 d-none d-md-flex bg-image"></div>

            </div>
        </div>
    );    
}