import type { Metadata } from "next";
import { League_Spartan } from "next/font/google";
import "bootstrap/dist/css/bootstrap.min.css";
import "@/styles/global.scss";
import "@/styles/icofont.css";

const inter = League_Spartan({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Homesport Bet",
  description: "Kenya's No 1 Betting site with awesome user experience",
};

export default function RootLayout({ children, }: Readonly<{ children: React.ReactNode; }>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        { children }
      </body>
    </html>
  );
}
